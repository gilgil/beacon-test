#include <GApp>
#include <GPcapDeviceWrite>
#include <GRadioHdr>
#include <GBeaconHdr>

GMac getRandomMac() {
	GMac mac;
	gbyte* b = pbyte(mac);
	for (int i = 0; i < GMac::Size; i++)
		*b++ = rand() % 256;
	return mac;
}

#pragma pack(push, 1)
struct BeaconFrame {
	GRadioHdr radioHdr_;
	GBeaconHdr beaconHdr_;
	char dummy[256];
	size_t size_;

	BeaconFrame(GMac mac, QString ssid) {
		//
		// radioHdr
		//
		radioHdr_.init();

		//
		// beaconHdr
		//
		GBeaconHdr* bh = &beaconHdr_;
		bh->ver_ = 0;
		bh->type_ = 0;
		bh->subtype_ = GDot11::Beacon;
		bh->flags_ = 0;
		bh->duration_ = 0;
		bh->addr1_ = GMac::broadcastMac();
		bh->addr2_ = mac;
		bh->addr3_ = mac;
		bh->frag_ = 0;
		bh->seq_ = 0; // gilgil temp 2022.10.27

		bh->fix_.timestamp_ = 0; // gilgil temp
		bh->fix_.beaconInterval_ = 0x6400;
		bh->fix_.capabilities_ = 0x0011; // gilgil temp

		// TagSsidParameterSet
		GBeaconHdr::Tag* tag = bh->firstTag();
		std::string _ssid = ssid.toStdString();
		tag->num_ = GBeaconHdr::TagSsidParameterSet;
		tag->len_ = _ssid.size();
		memcpy(pchar(tag->value()), _ssid.data(), _ssid.size());
		tag = tag->next();

		// TagSupportedRated
		tag->num_ = GBeaconHdr::TagSupportedRated;
		tag->len_ = 8;
		char* p = pchar(tag->value());
		*p++ = 0x82; // 1(B)
		*p++ = 0x84; // 2(B)
		*p++ = 0x8B; // 5.5(B)
		*p++ = 0x96; // 11(B)
		*p++ = 0x24; // 18
		*p++ = 0x30; // 24
		*p++ = 0x48; // 36
		*p++ = 0x6C; // 54
		tag = tag->next();

		// TagDsParameterSet
		tag->num_ = GBeaconHdr::TagDsParameterSet;
		tag->len_ = 1;
		*pchar(tag->value()) = 0x01; // channel 1
		tag = tag->next();

		// TagTrafficIndicationMap
		tag->num_ = GBeaconHdr::TagTrafficIndicationMap;
		tag->len_ = sizeof(GBeaconHdr::TrafficIndicationMap) - sizeof(GBeaconHdr::Tag);
		GBeaconHdr::TrafficIndicationMap* tim = GBeaconHdr::PTrafficIndicationMap(tag);
		tim->count_ = 0;
		tim->period_ = 3;
		tim->control_ = 0;
		tim->bitmap_ = 0;
		tag = tag->next();

		/*
		// TagHtCapabilities
		tag->num_ = GBeaconHdr::TagHtCapabilities;
		tag->len_ = sizeof(GBeaconHdr::HtCapabilities) - sizeof(GBeaconHdr::Tag);
		GBeaconHdr::HtCapabilities* hc = GBeaconHdr::PHtCapabilities(tag);
		hc->capabilitiesInfo_ = 0x012C;
		hc->mpduParameters_ = 0x1E;
		memset(hc->mcsSet_, 0x00, sizeof(hc->mpduParameters_));
		hc->mcsSet_[0] = 0xFF;
		tag = tag->next();

		// TagHtInformation
		tag->num_ = GBeaconHdr::TagHtInformation;
		tag->len_ = sizeof(GBeaconHdr::HtInformation) - sizeof(GBeaconHdr::Tag);
		GBeaconHdr::HtInformation* ht = GBeaconHdr::PHtInformation(tag);
		ht->primaryChannel_ = 1;
		ht->htInformationSubset1_ = 0;
		ht->htInformationSubset2_ = 0;
		ht->htInformationSubset3_ = 0;
		memset(ht->basicMcsSet_, 0x00, sizeof(ht->basicMcsSet_));
		tag = tag->next();

		// RSN Information
		char rsnInformation[] ="\x30\x14\x01\x00\x00\x0f\xac\x04\x01\x00\x00\x0f\xac\x04\x01\x00\x00\x0f\xac\x02\x00\x00";
		memcpy(tag, rsnInformation, sizeof(rsnInformation) - 1);
		tag = tag->next();

		// vendor specific
		char vendorSpecific[] = "\xdd\x18\x00\x50\xf2\x02\x01\x01\x80\x00\x03\xa4\x00\x00\x27\xa4\x00\x00\x42\x43\x5e\x00\x62\x32\x2f\x00";
		memcpy(tag, vendorSpecific, sizeof(vendorSpecific) - 1);
		tag = tag->next();
		*/

		// end
		size_ = pchar(tag) - pchar(this);
	}

	/*
	BeaconFrame(GMac mac, QString ssid) {
		//
		// radioHdr
		//
		radioHdr_.init();

		//
		// beaconHdr
		//
		GBeaconHdr* bh = &beaconHdr_;
		bh->ver_ = 0;
		bh->type_ = 0;
		bh->subtype_ = GDot11::Beacon;
		bh->flags_ = 0;
		bh->duration_ = 0;
		bh->addr1_ = GMac::broadcastMac();
		bh->addr2_ = mac;
		bh->addr3_ = mac;
		bh->frag_ = 0;
		bh->seq_ = 0; // gilgil temp 2022.10.27

		bh->fix_.timestamp_ = 0; // gilgil temp
		bh->fix_.beaconInterval_ = 0x6400;
		bh->fix_.capabilities_ = 0x0011; // gilgil temp

		// TagSsidParameterSet
		GBeaconHdr::Tag* tag = bh->firstTag();
		tag->num_ = GBeaconHdr::TagSsidParameterSet;
		tag->len_ = ssid.length();
		strcpy(pchar(tag->value()), qPrintable(ssid));
		tag = tag->next();

		// Tag: Supported Rates 1(B), 2(B), 5.5(B), 6, 9, 11(B), 12, 18, [Mbit/sec]
		char supportedRates[] = "\x01\x08\x82\x84\x8b\x0c\x12\x96\x18\x24";
		memcpy(tag, supportedRates, sizeof(supportedRates) - 1);
		tag = tag->next();

		// Tag: DS Parameter set: Current Channel: 1
		char dsParameterSet[] = "\x03\x01\x01";
		memcpy(tag, dsParameterSet, sizeof(dsParameterSet) - 1);
		tag = tag->next();

		// Tag: Extended Supported Rates 24, 36, 48, 54, [Mbit/sec]
		char extended[] = "\x32\x04\x30\x48\x60\x6c";
		memcpy(tag, extended, sizeof(extended) - 1);
		tag = tag->next();

		// Tag: Traffic Indication Map (TIM): DTIM 0 of 1 bitmap
		char tim[] = "\x05\x04\x00\x01\x00\x00";
		memcpy(tag, tim, sizeof(tim) - 1);
		tag = tag->next();

		// Tag: ERP Information
		char erp[] = "\x2a\x01\x06";
		memcpy(tag, erp, sizeof(erp) - 1);
		tag = tag->next();

		// Tag: Extended Supported Rates 24, 36, 48, 54, [Mbit/sec]
		char esp[] = "\x32\x04\x30\x48\x60\x6c";
		memcpy(tag, esp, sizeof(esp) - 1);
		tag = tag->next();

		// Tag: RSN Information
		char rsn[] = "\x30\x14\x01\x00\x00\x0f\xac\x04\x01\x00\x00\x0f\xac\x04\x01\x00\x00\x0f\xac\x02\x0c\x00";
		memcpy(tag, rsn, sizeof(rsn) - 1);
		tag = tag->next();

		// Tag: HT Capabilities (802.11n D1.10)
		char htc[] = "\x2d\x1a\xad\x01\x1f\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";
		memcpy(tag, htc, sizeof(htc) - 1);
		tag = tag->next();

		// Tag: HT Information (802.11n D1.10)
		char hti[] = "\x3d\x16\x01\x00\x11\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";
		memcpy(tag, hti, sizeof(hti) - 1);
		tag = tag->next();

		// Tag: Vendor Specific: Microsoft Corp.: WMM/WME: Parameter Element
		char vendor[] = "\xdd\x18\x00\x50\xf2\x02\x01\x01\x81\x00\x03\xa4\x00\x00\x27\xa4\x00\x00\x42\x43\x5e\x00\x62\x32\x2f\x00";
		memcpy(tag, vendor, sizeof(vendor) - 1);
		tag = tag->next();

		// Tag: Extended Capabilities (8 octets)
		char ec[] = "\x7f\x08\x04\x00\x00\x00\x00\x00\x00\x00";
		memcpy(tag, ec, sizeof(ec) - 1);
		tag = tag->next();

		// Tag: Vendor Specific: Samsung Electronics Co.,Ltd
		char samsung[] = "\xdd\x17\x00\x00\xf0\x0f\x00\x02\x09\x01\x04\x02\x01\x0d\x03\x09\x01\x07\x01\x02\x00\x01\x02\x01\x03";
		memcpy(tag, samsung, sizeof(samsung) - 1);
		tag = tag->next();

		// end
		size_ = pchar(tag) - pchar(this);
	}
	*/
};
#pragma pack(pop)

struct BeaconFrameList : QList<BeaconFrame> {};

struct BeaconInfo {
	GMac mac_;
	QString ssid_;
};

int main(int argc, char *argv[])
{
	GApp a(argc, argv);
	GPcapDeviceWrite device;
	device.intfName_ = "mon0";
	if (!device.open()) {
		qWarning() << device.err->msg();
		return -1;
	}

	BeaconFrameList bfl;
	bfl.push_back(BeaconFrame(GMac("28:6d:97:8a:11:79"), "test00"));
	bfl.push_back(BeaconFrame(GMac("02:11:22:33:44:03"), "가나다01"));
	/*
	bfl.push_back(BeaconFrame(GMac("02:11:22:33:44:01"), "test03"));
	bfl.push_back(BeaconFrame(GMac("02:11:22:33:44:02"), "test04"));
	bfl.push_back(BeaconFrame(GMac("02:11:22:33:44:04"), "test04"));
	bfl.push_back(BeaconFrame(GMac("02:11:22:33:44:05"), "test05"));
	bfl.push_back(BeaconFrame(GMac("02:11:22:33:44:06"), "test06"));
	bfl.push_back(BeaconFrame(GMac("02:11:22:33:44:07"), "test07"));
	bfl.push_back(BeaconFrame(GMac("02:11:22:33:44:08"), "test08"));
	bfl.push_back(BeaconFrame(GMac("02:11:22:33:44:09"), "test09"));

	bfl.push_back(BeaconFrame(GMac("00:11:22:33:45:00"), "test10"));
	bfl.push_back(BeaconFrame(GMac("00:11:22:33:45:01"), "test11"));
	bfl.push_back(BeaconFrame(GMac("00:11:22:33:45:02"), "test12"));
	bfl.push_back(BeaconFrame(GMac("00:11:22:33:45:03"), "test13"));
	bfl.push_back(BeaconFrame(GMac("00:11:22:33:45:04"), "test14"));
	bfl.push_back(BeaconFrame(GMac("00:11:22:33:45:05"), "test15"));
	bfl.push_back(BeaconFrame(GMac("00:11:22:33:45:06"), "test16"));
	bfl.push_back(BeaconFrame(GMac("00:11:22:33:45:07"), "test17"));
	bfl.push_back(BeaconFrame(GMac("00:11:22:33:45:08"), "test18"));
	bfl.push_back(BeaconFrame(GMac("00:11:22:33:45:09"), "test19"));
	*/

	while (true) {
		bool ok = true;
		for (BeaconFrame& bf: bfl) {
			//GMac mac = getRandomMac();
			//bf.beaconHdr_.addr2_ = mac;
			//bf.beaconHdr_.addr3_ = mac;
			GBuf buf(pbyte(&bf), bf.size_);
			GPacket::Result res = device.write(buf);
			if (res != GPacket::Ok) {
				qWarning() << device.err->msg();
				ok = false;
				break;
			}
			QThread::msleep(1);
		}
		if (!ok) break;
		QThread::msleep(50 - bfl.count());
	}

	return a.exec();
}
